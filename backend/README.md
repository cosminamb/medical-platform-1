# spring-app
An online platform designed to manage patients, caregivers and medication. The system can be accessed by three types of users after a login process: doctor, patient and caregiver, each of them having different roles and responsabilities.
