package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.repositories.CaregiverRepository;
import ro.tuc.ds2020.repositories.MedicationPlanRepository;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;
    private final CaregiverRepository caregiverRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository, CaregiverRepository caregiverRepository, MedicationPlanRepository medicationPlanRepository) {
        this.patientRepository = patientRepository;
        this.caregiverRepository = caregiverRepository;
        this.medicationPlanRepository = medicationPlanRepository;
    }

    public List<PatientDTO> findPatients(){
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public PatientDetailsDTO findPatientById(UUID id){
        Optional<Patient> optionalPatient = patientRepository.findById(id);
        if(!optionalPatient.isPresent()){
            LOGGER.error("Patient with id {} was not found in db", id);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + id);
        }
        return PatientBuilder.toPatientDetailsDTO(optionalPatient.get());
    }

    public UUID insert(PatientDetailsDTO patientDTO) {
        Patient patient = PatientBuilder.toEntity(patientDTO);
        Caregiver caregiver = null;
        if(patientDTO.hasCaregiverId()){
            caregiver = caregiverRepository.findCaregiverById(patientDTO.getCaregiverId()); //?
            patient.setCaregiver(caregiver);
        }
        if(patientDTO.hasMedicationPlanId()){
            Optional<MedicationPlan> optionalMedicationPlan = medicationPlanRepository.findById(patientDTO.getMedicationPlanId());
            if (!optionalMedicationPlan.isPresent()) {
                LOGGER.error("MedicationPlan with id {} was not found in db", patientDTO.getMedicationPlanId());
                throw new ResourceNotFoundException(MedicationPlan.class.getSimpleName() + " with id: " + patientDTO.getMedicationPlanId());
            }
            MedicationPlan medicationPlan = optionalMedicationPlan.get();
            patient.setMedicationPlan(medicationPlan);
        }
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public UUID deletePatientById(UUID patientId) {
        Optional<Patient> medicationOptional = patientRepository.findById(patientId);
        if (!medicationOptional.isPresent()) {
            LOGGER.error("Patient with id {} was not found in db", patientId);
            throw new ResourceNotFoundException(Patient.class.getSimpleName() + " with id: " + patientId);
        }
        patientRepository.deleteById(patientId);

        LOGGER.debug("Patient with id {} was deleted ", patientId);

        return patientId;
    }

    public PatientDetailsDTO updatePatient(PatientDetailsDTO patientDTO) {
        Patient fromDB = patientRepository.findPatientById(patientDTO.getId());
        fromDB.setName(patientDTO.getName());
        fromDB.setAddress(patientDTO.getAddress());
        fromDB.setGender(patientDTO.getGender());
        fromDB.setAge(patientDTO.getAge());
        fromDB.setBirthdate(patientDTO.getBirthdate());
        fromDB.setMedicalRecord(patientDTO.getMedicalRecord());
        Caregiver caregiver = null;
        if(patientDTO.getCaregiverId() != null){
            caregiver = caregiverRepository.findCaregiverById(patientDTO.getCaregiverId());
            fromDB.setCaregiver(caregiver);
        }
        Patient patient = patientRepository.save(fromDB);
        return PatientBuilder.toPatientDetailsDTO(patient);
    }


}
