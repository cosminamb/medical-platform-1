package ro.tuc.ds2020.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Person implements Serializable {

    private static final long serialVersionUID = -3624101038666734797L;
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(columnDefinition = "uuid")
    private UUID id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "address", nullable = false)
    private String address;

    @Column(name = "age", nullable = false)
    private int age;

    @Column(name = "birthdate", nullable = false)
    private Date birthdate;

    @Column(name = "gender", nullable = false)
    private char gender;

    @OneToOne(mappedBy = "accountType")
    private Account account;


    public Person(String name, String address, int age, Date birthdate, char gender) {
        this.name = name;
        this.address = address;
        this.age = age;
        this.birthdate = birthdate;
        this.gender = gender;
    }

    public Person(UUID id, String name, String address, int age, Date birthdate, char gender) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.age = age;
        this.birthdate = birthdate;
        this.gender = gender;
    }
}
