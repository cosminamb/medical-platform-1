package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.AccountDetailsDTO;
import ro.tuc.ds2020.dtos.AccountTypeDTO;
import ro.tuc.ds2020.security.JwtTokenUtil;
import ro.tuc.ds2020.services.AccountService;

import java.util.ArrayList;

@RestController
@CrossOrigin
@RequestMapping(value = "/login")
public class LoginController {

    private final AccountService accountService;

    @Autowired
    public LoginController(AccountService accountService) {
        this.accountService = accountService;
    }


    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @PostMapping
    public ResponseEntity<AccountTypeDTO> login(@RequestBody AccountDetailsDTO accountDetailsDTO) {
        AccountDetailsDTO userFromDB = accountService.findAccount(accountDetailsDTO.getUsername(), accountDetailsDTO.getPassword());
        AccountTypeDTO toReturn = accountService.getAccountType(userFromDB);
        String token = jwtTokenUtil.generateToken(new User(userFromDB.getUsername(), userFromDB.getPassword(), new ArrayList<>()));
        toReturn.setToken(token);
        ResponseEntity<AccountTypeDTO> responseEntity = new ResponseEntity<>(toReturn, HttpStatus.OK);
        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getBody().getToken());
        return responseEntity;
    }
}
