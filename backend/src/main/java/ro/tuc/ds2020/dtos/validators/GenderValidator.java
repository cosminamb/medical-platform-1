package ro.tuc.ds2020.dtos.validators;

import org.springframework.stereotype.Component;
import ro.tuc.ds2020.dtos.validators.annotation.GenderType;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class GenderValidator implements ConstraintValidator<GenderType, Character> {

    @Override
    public void initialize(GenderType constraintAnnotation) {

    }

    @Override
    public boolean isValid(Character c, ConstraintValidatorContext constraintValidatorContext) {
        return c.equals('M') || c.equals('F');
    }
}
