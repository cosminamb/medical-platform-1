package ro.tuc.ds2020.dtos;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.hateoas.RepresentationModel;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public class PersonDTO extends RepresentationModel<PersonDTO> {
    private UUID id;
    private String name;
    private int age;
    private Date birthdate;
    private char gender;
    private String address;


    public PersonDTO(UUID id, String name, int age, Date birthdate, String address, char gender) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonDTO personDTO = (PersonDTO) o;
        return age == personDTO.age &&
                Objects.equals(name, personDTO.name) &&
                Objects.equals(birthdate, personDTO.birthdate) &&
                Objects.equals(gender, personDTO.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
