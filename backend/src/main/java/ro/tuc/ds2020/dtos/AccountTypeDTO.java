package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class AccountTypeDTO {

    private PersonDTO person;
    private String accTy;

    private String token;

    public AccountTypeDTO(String accTy) {
        this.accTy = accTy;
    }

    public AccountTypeDTO(PersonDTO person, String accTy) {
        this.person = person;
        this.accTy = accTy;
    }
}
