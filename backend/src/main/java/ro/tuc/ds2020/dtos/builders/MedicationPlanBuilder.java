package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.dtos.MedicationPlanIdDTO;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.MedicationPlan;

import java.util.ArrayList;
import java.util.List;

public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medicationPlan) {
        List<MedicationDTO> medicationDTOS = new ArrayList<>();
        for (Medication med : medicationPlan.getMedicationList()) {
            medicationDTOS.add(MedicationBuilder.toMedicationDTO(med));
        }
        return new MedicationPlanDTO(medicationPlan.getId(), medicationPlan.getIntakeInterval(),
                medicationPlan.getTreatmentPeriod(), medicationDTOS);
    }

    public static MedicationPlanDetailsDTO toMedicationPlanDetailsDTO(MedicationPlan medicationPlan) {
        List<MedicationDTO> medications = new ArrayList<>();
        if (!medicationPlan.getMedicationList().isEmpty()) {
            for (Medication med : medicationPlan.getMedicationList()) {
                medications.add(MedicationBuilder.toMedicationDTO(med));
            }
        }
        return new MedicationPlanDetailsDTO(medicationPlan.getId(), medicationPlan.getIntakeInterval(), medicationPlan.getTreatmentPeriod(), medications);
    }

    public static MedicationPlan toEntity(MedicationPlanDetailsDTO medicationPlanDetailsDTO) {
        return new MedicationPlan(medicationPlanDetailsDTO.getId(),
                medicationPlanDetailsDTO.getIntakeInterval(),
                medicationPlanDetailsDTO.getTreatmentPeriod());
    }

    public static MedicationPlan toEntityFromId(MedicationPlanIdDTO medicationPlanIdDTO) {
        return new MedicationPlan(
                medicationPlanIdDTO.getId(),
                medicationPlanIdDTO.getIntakeInterval(),
                medicationPlanIdDTO.getTreatmentPeriod()
        );
    }
}
