package ro.tuc.ds2020.dtos.validators.annotation;

import ro.tuc.ds2020.dtos.validators.GenderValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = {GenderValidator.class})
public @interface GenderType {

    String message() default "Wrong gender input. Only 'M' of 'F' values are allowed";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
