package ro.tuc.ds2020.services;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;
import ro.tuc.ds2020.dtos.DoctorDTO;

import java.util.List;

import static org.springframework.test.util.AssertionErrors.assertEquals;


@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class DoctorServiceIntegrationTest extends Ds2020TestConfig {


    @Autowired
    DoctorService doctorService;

    @Test
    public void testGetCorrect() {
        List<DoctorDTO> doctorDTOList = doctorService.findDoctors();
        assertEquals("Test Insert Doctor", 1, doctorDTOList.size());
    }

//    @Test
//    public void testInsertCorrectWithGetById() {
//        DoctorDetailsDTO p = new DoctorDetailsDTO("John", "Somewhere Else street", 22, new Date(1998, 12, 06), 'M');
//        UUID insertedID = doctorService.insert(p);
//
//        DoctorDetailsDTO insertedDoctor = new DoctorDetailsDTO(insertedID, p.getName(),p.getAddress(), p.getAge(), p.getBirthdate(), p.getGender());
//        DoctorDetailsDTO fetchedDoctor = doctorService.findDoctorById(insertedID);
//
//        assertEquals("Test Inserted Doctor", insertedDoctor, fetchedDoctor);
//    }
//
//    @Test
//    public void testInsertCorrectWithGetAll() {
//        DoctorDetailsDTO p = new DoctorDetailsDTO("John", "Somewhere Else street", 22, new Date(1998, 12, 06), 'M');
//        doctorService.insert(p);
//
//        List<DoctorDTO> doctorDTOList = doctorService.findDoctors();
//        assertEquals("Test Inserted Doctors", 2, doctorDTOList.size());
//    }

}