package ro.tuc.ds2020.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class PersonServiceIntegrationTests extends Ds2020TestConfig {

    @Autowired
    PersonService personService;

    /*
    @Test
    public void testGetCorrect() {
        List<PersonDTO> personDTOList = personService.findPersons();
        assertEquals("Test Insert Person", 4, personDTOList.size()); //changes depending on other insert tests
    }

     */

//    @Test
//    public void testInsertCorrectWithGetById() {
//        PersonDetailsDTO p = new PersonDetailsDTO("John", "Somewhere Else street", 22, new Date(1998, 12, 06), 'M');
//        UUID insertedID = personService.insert(p);
//
//        PersonDetailsDTO insertedPerson = new PersonDetailsDTO(insertedID, p.getName(),p.getAddress(), p.getAge(), p.getBirthdate(), p.getGender());
//        PersonDetailsDTO fetchedPerson = personService.findPersonById(insertedID);
//
//        assertEquals("Test Inserted Person", insertedPerson, fetchedPerson);
//    }

    /*
    @Test
    public void testInsertCorrectWithGetAll() {
        PersonDetailsDTO p = new PersonDetailsDTO("John", "Somewhere Else street", 22, new Date(1998, 12, 06), 'M');
        personService.insert(p);

        List<PersonDTO> personDTOList = personService.findPersons();
        assertEquals("Test Inserted Persons", 5, personDTOList.size()); //changes depending on other insert tests
    }

     */
}
