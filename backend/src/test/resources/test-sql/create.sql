INSERT INTO person (id, name, address, age, birthdate, gender) values
    ('00201D1E61F3401588BF4292B86E22E4', 'My Name', 'My Address', 20, '2000-08-09', 'M');

INSERT INTO caregiver (id, name, address, age, birthdate, gender) values
    ('00435C913AF3391588BF4292B86E17B9', 'My Name', 'My Address', 20, '2000-08-09', 'M');

INSERT INTO patient (id, name, address, age, birthdate, gender, medical_record) values
    ('00445D1E61F3362691BF4292B86E11E4', 'My Name', 'My Address', 20, '2000-08-09', 'M', 'None');

INSERT INTO doctor (id, name, address, age, birthdate, gender) values
    ('00495D1E68F3271588BF4291C96E11E4', 'My Name', 'My Address', 20, '2000-08-09', 'M');


