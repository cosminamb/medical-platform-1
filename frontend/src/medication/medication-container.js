import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';
import MedicationForm from "./components/medication-form";

import * as API_USERS from "./api/medication-api"
import MedicationTable from "./components/medication-table";
import MedicationUpdateForm from "./components/medication-form-update";


class MedicationContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.toggleAddModal = this.toggleAddModal.bind(this);

        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            isOpenModalUpdate: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
        this.fetchMedications();
    }

    fetchMedications() {
        return API_USERS.getMedications((result, status, err) => {
            console.log("result table");
            console.log(result);
            if (result !== null && status === 200) {
                this.setState({
                    tableData: result,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: err
                }));
            }
        });
    }

    toggleAddModal(rowData) {
        this.setState({
                isOpenModalUpdate: !this.state.isOpenModalUpdate,
                rowData: rowData
            }
        );
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchMedications();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Medication Management </strong>
                </CardHeader>
                <Card>
                    <br/>

                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add Medication </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <MedicationTable
                                tableData={this.state.tableData}
                                toggleAddModal={this.toggleAddModal}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>

                    <br/>
                    <br/>
                    <br/>


                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationForm reloadHandler={this.reload}/>
                    </ModalBody>
                </Modal>

                <Modal isOpen={this.state.isOpenModalUpdate} toggle={this.toggleAddModal}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleAddModal}> Edit Medication: </ModalHeader>
                    <ModalBody>
                        <MedicationUpdateForm
                            reloadHandler={this.reload}
                            rowData={this.state.rowData}
                        />
                    </ModalBody>
                </Modal>


            </div>
        )

    }
}


export default MedicationContainer;
