import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";

const endpoint = {
    medication: '/medication'
};

function getMedications(callback) {
    let request = new Request( endpoint.medication, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);

}


function getMedicationById(params, callback) {
    let request = new Request( endpoint.medication + params.id, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function deleteMedicationById(id, callback) {
    let request = new Request( endpoint.medication + '/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function updateMedicationById(medication, callback) {
    console.log("JSON.stringify(medication))")
    let request = new Request( endpoint.medication, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
            // "Access-Control-Allow-Origin": "*",
            // "Access-Control-Allow-Origin": "http://localhost.com:3000",
        },
        body: JSON.stringify(medication)


    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedication(user, callback) {
    let request = new Request( endpoint.medication, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedications,
    getMedicationById,
    postMedication,
    deleteMedicationById,
    updateMedicationById
};