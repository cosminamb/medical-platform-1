import React from 'react';
import {Card, CardHeader, Col, Row} from 'reactstrap';
import * as API_USERS_MP from "../medication_plan/medication-plan-api";
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import PatientMedicationTable from "./components/patients-medications-table";

class PatientMyPageContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null,
            myNameP: null,
            myAgeP: null,
            myAddress: null,
            myBirthdate: null,
            myGender: null,
            myMedicalRecord: null,
            MPintakeInterval: null,
            MPdosage: null,
            MPid: null,

        };
        this.fetchDetails();
    }

    componentDidMount() {
        this.fetchDetails();

    }

    fetchDetails() {
        // console.log("am ajuns aici");
        // console.log(localStorage.getItem('token'));
        // console.log(localStorage.getItem('details'));
        let myobject = localStorage.getItem('details');
        let stringName = '';
        let stringAge = '';
        let stringAddress = '';
        let stringBirthdate = '';
        let stringGender = '';
        let stringMedicalRecord = '';
        let firstEnter = 1;
        JSON.parse(myobject, (key, val) => {
            switch (key) {
                case "name":
                    if (firstEnter) {
                        stringName = val;
                        firstEnter = 0;
                    }
                    break;
                case "age":
                    stringAge = val;
                    break;
                case "address":
                    stringAddress = val;
                    break;
                case "birthdate":
                    stringBirthdate = val.substring(0, 10);
                    break;
                case "gender":
                    stringGender = val;
                    break;
                case "medicalRecord":
                    stringMedicalRecord = val;
                    break;
                default:
                    break;
            }
        });
        this.state.myNameP = stringName;
        this.state.myAge = stringAge;
        this.state.myAddress = stringAddress;
        this.state.myBirthdate = stringBirthdate;
        this.state.myGender = stringGender;
        this.state.myMedicalRecord = stringMedicalRecord;
        if (JSON.parse(myobject).medicationPlanDTO !== null) {
            this.state.MPdosage = JSON.parse(myobject).medicationPlanDTO.treatmentPeriod;
            this.state.MPintakeInterval = JSON.parse(myobject).medicationPlanDTO.intakeInterval;
            this.state.MPid = JSON.parse(myobject).medicationPlanDTO.id;
            this.getMedications(this.state.MPid);
        }
    }

    getMedications(id) {
        return API_USERS_MP.getMedicationPlanById(id, (result, status, error) => {
            if (result !== null && (status === 200 || status === 201)) {
                console.log("Successfully got medication plan ");
                console.log(result.medicationIds);
                this.setState({
                    tableData: result.medicationIds,
                    isLoaded: true
                });
            } else {
                this.setState(({
                    errorStatus: status,
                    error: error
                }));
            }
        });
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
    }

    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.fetchDetails();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Management </strong>
                    <br/>
                    <br/>
                    <p> Name: {this.state.myNameP}</p>
                    <p> Age: {this.state.myAge}</p>
                    <p> Birth date: {this.state.myBirthdate}</p>
                    <p> Gender: {this.state.myGender}</p>
                    <p> Address: {this.state.myAddress}</p>
                    <p> Medical record: {this.state.myMedicalRecord}</p>
                </CardHeader>
                <Card>
                    <br/>
                    <p><strong> My medication plan: </strong></p>
                    <p> Interval: {this.state.MPintakeInterval} </p>
                    <p> Treatment Period: {this.state.MPdosage} </p>


                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {this.state.isLoaded && <PatientMedicationTable
                                tableData={this.state.tableData}
                            />}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                    <br/>
                </Card>
            </div>
        )

    }
}

export default PatientMyPageContainer;
