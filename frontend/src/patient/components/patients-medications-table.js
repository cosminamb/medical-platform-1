import React from "react";
import Table from "../../commons/tables/table";


const filters = [
    {
        accessor: 'name',
    }
];

class PatientMedicationTable extends React.Component {

    constructor(props) {
        super(props);
        const columns = [
            {
                Header: 'Id',
                accessor: 'id',
            },
            {
                Header: 'Name',
                accessor: 'name',
            },
            {
                Header: 'Side Effects',
                accessor: 'sideEffects',
            },
            {
                Header: 'Dosage',
                accessor: 'dosage',
            }];


        this.state = {
            tableData: this.props.tableData,
            columns: columns,
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={this.state.columns}
                search={filters}
                pageSize={5}
            >
            </Table>
        )
    }

}

export default PatientMedicationTable;



