import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient'
};

function getPatients(callback) {
    console.log('auth');
    console.log(localStorage.getItem('auth'))
    let request = new Request(endpoint.patient, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);

}


function getPatientById(id, callback) {
    let request = new Request(endpoint.patient + '/' + id, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function deletePatientById(id, callback) {
    let request = new Request( endpoint.patient + '/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function updatePatientById(patient, callback) {
    let request = new Request(endpoint.patient, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
        body: JSON.stringify(patient)


    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postPatient(user, callback) {
    let request = new Request(endpoint.patient, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getPatients,
    getPatientById,
    postPatient,
    deletePatientById,
    updatePatientById,
};