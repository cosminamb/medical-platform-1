import * as React from "react";
import Table from "../../commons/tables/table";


const columns = [
    {
        Header: 'Username',
        accessor: 'username',
    },
    {
        Header: 'Password',
        accessor: 'password',
    }
];

class LoginTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            tableData: this.props.tableData
        };
    }

    render() {
        return (
            <Table
                data={this.state.tableData}
                columns={columns}
                pageSize={5}
            />
        )
    }

}

export default LoginTable;