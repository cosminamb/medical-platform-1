import React from 'react';
import {Card, CardHeader} from 'reactstrap';
import LoginForm from "./components/login-form";


class LoginContainer extends React.Component {

    constructor(props) {
        super(props);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
    }

    componentDidMount() {
    }

    reload() {
        this.setState({
            isLoaded: false
        });
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> Input your details to login: </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <br/>
                    <br/>
                    <LoginForm reloadHandler={this.reload}/>
                    <br/>
                </Card>
            </div>
        )

    }
}


export default LoginContainer;
