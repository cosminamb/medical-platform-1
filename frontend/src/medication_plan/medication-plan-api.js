import {HOST} from '../commons/hosts';
import RestApiClient from "../commons/api/rest-client";


const endpoint = {
    medicationPlan: '/medicationPlan'
};

function getMedicationPlans(callback) {
    let request = new Request( endpoint.medicationPlan, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);

}

function getMedicationPlanById(id, callback) {
    let request = new Request( endpoint.medicationPlan + '/' + id, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function deleteMedicationPlanById(id, callback) {
    let request = new Request( endpoint.medicationPlan + '/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function updateMedicationPlanById(medicationPlan, callback) {
    let request = new Request( endpoint.medicationPlan, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
        body: JSON.stringify(medicationPlan)

    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function postMedicationPlan(user, callback) {
    let request = new Request( endpoint.medicationPlan, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

export {
    getMedicationPlans,
    getMedicationPlanById,
    postMedicationPlan,
    deleteMedicationPlanById,
    updateMedicationPlanById,
};