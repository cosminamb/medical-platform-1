import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    caregiver: '/caregiver'
};

function getCaregivers(callback) {
    let request = new Request( endpoint.caregiver, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);

}


function getCaregiverById(params, callback) {
    let request = new Request( endpoint.caregiver + '/' + params, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function deleteCaregiverById(id, callback) {
    let request = new Request( endpoint.caregiver + '/' + id, {
        method: 'DELETE',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        }
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function updateCaregiverById(caregiver, callback) {
    let request = new Request( endpoint.caregiver, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
        body: JSON.stringify(caregiver)


    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}


function postCaregiver(user, callback) {
    let request = new Request( endpoint.caregiver, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + localStorage.getItem('auth'),
        },
        body: JSON.stringify(user)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}


export {
    getCaregivers,
    getCaregiverById,
    postCaregiver,
    deleteCaregiverById,
    updateCaregiverById,

};